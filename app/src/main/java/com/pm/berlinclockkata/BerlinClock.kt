package com.pm.berlinclockkata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BerlinClock : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun singleMinuteRow(time: String): String {
        val c = parseDateString(time)!!
        val data = getClock(c.get(Calendar.HOUR_OF_DAY), c[Calendar.MINUTE], c[Calendar.SECOND])!!
        val row = StringBuilder()
        for (i in 0..3) {
            val state = if (data[0] >= i + 1) "Y" else "O"
            row.append(state)
        }
        return row.toString()
    }

    fun fiveMinuteRow(time: String): String {
        val c = parseDateString(time)!!
        val data = getClock(c.get(Calendar.HOUR_OF_DAY), c[Calendar.MINUTE], c[Calendar.SECOND])!!
        val row = StringBuilder()
        for (i in 1..11) {
            var state = if (data[1] >= i) "Y" else "O"
            if (i % 3 == 0 && state == "Y") {
                state = "R"
            }
            row.append(state)
        }
        return row.toString()
    }

    fun singleHourRow(time: String): String {
        val c = parseDateString(time)!!
        val data = getClock(c.get(Calendar.HOUR_OF_DAY), c[Calendar.MINUTE], c[Calendar.SECOND])!!
        val row = StringBuilder()
        for (i in 0..3) {
            val state = if (data[2] >= i + 1) "R" else "O"
            row.append(state)
        }
        return row.toString()
    }

    fun fiveHourRow(time: String): String {
        val c = parseDateString(time)!!
        val data = getClock(c.get(Calendar.HOUR_OF_DAY), c[Calendar.MINUTE], c[Calendar.SECOND])!!
        val row = StringBuilder()
        for (i in 0..3) {
            val state = if (data[3] >= i + 1) "R" else "O"
            row.append(state)
        }
        return row.toString()
    }

    fun secondsLamp(time: String): String {
        val c = parseDateString(time)!!
        val data = getClock(c.get(Calendar.HOUR_OF_DAY), c[Calendar.MINUTE], c[Calendar.SECOND])!!
        return if (data[4] == 0) "Y" else "O"
    }

    fun entireBerlinClock(time: String): String {
        return secondsLamp(time)
                .plus(fiveHourRow(time))
                .plus(singleHourRow(time))
                .plus(fiveMinuteRow(time))
                .plus(singleMinuteRow(time))
    }

    fun convertBerlinTimeToDigitalTime (time: String): String? {
        val fiveHours: String = time.substring(1, 5)
        val singleHours: String = time.substring(5, 9)
        val fiveMinutes: String = time.substring(9, 20)
        val singleMinutes: String = time.substring(20, 24)

        var hours = 0
        var minutes = 0

        for (i in fiveHours.indices) {
            if(fiveHours[i] == 'R') hours += 5
        }
        for (i in singleHours.indices) {
            if(singleHours[i] == 'R') hours += 1
        }

        for (i in fiveMinutes.indices) {
            if(fiveMinutes[i] == 'R' || fiveMinutes[i] == 'Y') minutes += 5
        }

        for (i in singleMinutes.indices) {
            if(singleMinutes[i] == 'Y') minutes += 1
        }

        val hoursText = if(hours == 0) "00" else hours.toString()
        val minutesText = if(minutes == 0) "00" else minutes.toString()

        return "$hoursText:$minutesText"
    }

    private fun getClock(hours: Int, minutes: Int, seconds: Int): IntArray? {
        val lights = IntArray(5)
        lights[0] = minutes % 5
        lights[1] = minutes / 5
        lights[2] = hours % 5
        lights[3] = hours / 5
        lights[4] = seconds % 2
        return lights
    }

    private fun parseDateString(time: String): Calendar? {
        val sdf = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val cal = Calendar.getInstance()
        try {
            cal.time = sdf.parse(time)!!
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return cal
    }
}