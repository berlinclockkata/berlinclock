package com.pm.berlinclockkata

import org.junit.Test
import org.junit.Assert.*

class BerlinClockTest {

    private val berlinClock = BerlinClock()

    @Test
    fun singleMinutesRowTest() {

        val time1 = berlinClock.singleMinuteRow("00:00:00")
        val time2 = berlinClock.singleMinuteRow("23:59:59")
        val time3 = berlinClock.singleMinuteRow("12:32:00")
        val time4 = berlinClock.singleMinuteRow("12:34:00")
        val time5 = berlinClock.singleMinuteRow("12:35:00")

        assertEquals("OOOO", time1)
        assertEquals("YYYY", time2)
        assertEquals("YYOO", time3)
        assertEquals("YYYY", time4)
        assertEquals("OOOO", time5)
    }

    @Test
    fun fiveMinutesRowTest() {
        val time1 = berlinClock.fiveMinuteRow("00:00:00")
        val time2 = berlinClock.fiveMinuteRow("23:59:59")
        val time3 = berlinClock.fiveMinuteRow("12:04:00")
        val time4 = berlinClock.fiveMinuteRow("12:23:00")
        val time5 = berlinClock.fiveMinuteRow("12:35:00")

        assertEquals("OOOOOOOOOOO", time1)
        assertEquals("YYRYYRYYRYY", time2)
        assertEquals("OOOOOOOOOOO", time3)
        assertEquals("YYRYOOOOOOO", time4)
        assertEquals("YYRYYRYOOOO", time5)
    }

    @Test
    fun singleHoursRowTest() {
        val time1 = berlinClock.singleHourRow("00:00:00")
        val time2 = berlinClock.singleHourRow("23:59:59")
        val time3 = berlinClock.singleHourRow("02:04:00")
        val time4 = berlinClock.singleHourRow("08:23:00")
        val time5 = berlinClock.singleHourRow("14:35:00")

        assertEquals("OOOO", time1)
        assertEquals("RRRO", time2)
        assertEquals("RROO", time3)
        assertEquals("RRRO", time4)
        assertEquals("RRRR", time5)
    }

    @Test
    fun fiveHoursRowTest() {
        val time1 = berlinClock.fiveHourRow("00:00:00")
        val time2 = berlinClock.fiveHourRow("23:59:59")
        val time3 = berlinClock.fiveHourRow("02:04:00")
        val time4 = berlinClock.fiveHourRow("08:23:00")
        val time5 = berlinClock.fiveHourRow("16:35:00")

        assertEquals("OOOO", time1)
        assertEquals("RRRR", time2)
        assertEquals("OOOO", time3)
        assertEquals("ROOO", time4)
        assertEquals("RRRO", time5)
    }

    @Test
    fun secondsLampTest() {
        val time1 = berlinClock.secondsLamp("00:00:00")
        val time2 = berlinClock.secondsLamp("23:59:59")

        assertEquals("Y", time1)
        assertEquals("O", time2)
    }

    @Test
    fun entireBerlinClockTest() {
        val time1 = berlinClock.entireBerlinClock("00:00:00")
        val time2 = berlinClock.entireBerlinClock("23:59:59")
        val time3 = berlinClock.entireBerlinClock("16:50:06")
        val time4 = berlinClock.entireBerlinClock("11:37:01")

        assertEquals("YOOOOOOOOOOOOOOOOOOOOOOO", time1)
        assertEquals("ORRRRRRROYYRYYRYYRYYYYYY", time2)
        assertEquals("YRRROROOOYYRYYRYYRYOOOOO", time3)
        assertEquals("ORROOROOOYYRYYRYOOOOYYOO", time4)
    }

    @Test
    fun convertBerlinTimeToDigitalTimeTest(){
        val time1 = berlinClock.convertBerlinTimeToDigitalTime("YOOOOOOOOOOOOOOOOOOOOOOO")
        val time2 = berlinClock.convertBerlinTimeToDigitalTime("ORRRRRRROYYRYYRYYRYYYYYY")
        val time3 = berlinClock.convertBerlinTimeToDigitalTime("YRRROROOOYYRYYRYYRYOOOOO")
        val time4 = berlinClock.convertBerlinTimeToDigitalTime("ORROOROOOYYRYYRYOOOOYYOO")

        assertEquals("00:00", time1)
        assertEquals("23:59", time2)
        assertEquals("16:50", time3)
        assertEquals("11:37", time4)
    }
}